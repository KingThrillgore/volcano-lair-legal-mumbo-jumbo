---
title: "Discord Moderation Log"
date: 2023-02-12T23:52:00-06:00
draft: false
---

# Discord Moderation Log

This document outlines all Moderation actions taken by the Discord Moderation and Adminstration Team, sorted in reverse chronological order.

The document shall be formatted in the following manner:

* The nature of the incident
* The time and date of the incident that transpired
* The moderator(s) or administrator(s) involved in the incident
* The offending parties involved
* The actions taken to resolve

This document is updated as the needs arise, and may not be in sync with moderation team actions.

----

